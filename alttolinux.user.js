// ==UserScript==
// @name        Alternativeto Linux/Open-source
// @description Sets platform to Linux and license to Opensource if one is not set on Alternativeto.net.
// @icon        https://alternativeto.net/favicon.ico
// @match       https://alternativeto.net/software/*
// @run-at      document-start
// @updateURL   https://framagit.org/hanzo/alternativeto-linux-opensource/raw/master/alttolinux.user.js
// ==/UserScript==


var url = new URL(window.location);
var params = new URLSearchParams(url.search);

if ( !params.has("license") || !params.has("platform")) {
    url.searchParams.set('license', 'opensource');
    url.searchParams.set('platform', 'linux');
    console.log(url.toString());
    window.location = url.toString();
}